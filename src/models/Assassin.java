package models;

public abstract class Assassin extends Hero {

    private final double ATTACK_MULTIPLIER = 0.3;
    private final double DEFENSE_MULTIPLIER = 0.3;

    @Override
    protected double getAttackMultiplier() {
        return ATTACK_MULTIPLIER;
    }

    @Override
    protected double getDefenseMultiplier() {
        return DEFENSE_MULTIPLIER;
    }
}
