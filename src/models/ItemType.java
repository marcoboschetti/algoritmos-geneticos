package models;

/**
 * Created by Marco on 5/24/2017.
 */
public enum ItemType {
    HELMET, BOOTS, WEAPON, CHESTPLATE, GLOBES
}
