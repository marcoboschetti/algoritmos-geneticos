package models;

import java.util.List;

public abstract class Hero {


    private List<Item> equipedItems;
    private double height;

    private Double fitness;

    protected final double ATTACK_MULTIPLIER = 0.3;
    private final double DEFENSE_MULTIPLIER = 0.3;

    public Double getFitness() {
        if (fitness == null) {
            fitness = calculateFitness();
        }
        return fitness;
    }

    protected abstract double getAttackMultiplier();
    protected abstract double getDefenseMultiplier();

    protected abstract double getAttack();
    protected abstract double getDefense();


    public Double calculateFitness(double attackMultiplier, double defenseMultiplier, double attack, double defense) {

        double attack = getAttack();
        double defense = getDefense();


        return getAttackMultiplier() * attack + getDefenseMultiplier() * defense;
    }

}
